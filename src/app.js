global.$ = require('jquery');
require('bootstrap');

$(document).ready(function () {
    $('.carousel').carousel();

    $('#currentYear').text((new Date).getFullYear());
});
